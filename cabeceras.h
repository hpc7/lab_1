/********************************************************************************/
/*					Departamento de Ingenieria Informatica 						*/
/*						Universidad de Santiago de Chile						*/
/*						    High Performance Computing		     			    */
/*						 	     Laboratorio 1									*/
/********************************************************************************/
/*		  Autores: Gustavo Hurtado Arévalo y Patricia Melo Fuenzalida.			*/
/********************************************************************************/



/*---------------------------------------------------------------------------------------------------
Función que valida los parámetros de entrada del getopt.
Entrada: Parámetros del getopt: nombre archivo imagen entrada y salida, número de filas, de columnas, de ángulos, de distancias y del umbral. 
Salida: Nada.
*/
int validateEntry(char *image_raw_str, char *image_hough_str, int rows, int columns, int angles, int distances, int threshold);

/*---------------------------------------------------------------------------------------------------
Funcion que entrega memoria a una matriz.
Entrada: Una matriz, filas y columnas de la misma matriz.
Salida: Matriz con memoria.
*/
int** giveMemorieMatrix(int rows, int columns);

/*---------------------------------------------------------------------------------------------------
Funcion que lee el archivo de imagen.
Entrada: Una matriz, nombre archivo imagen, número de filas y columnas.
Salida: Matriz con imagen.
*/
int** readImage(int **image_matrix, char *image_raw_str, int rows, int columns);

/*---------------------------------------------------------------------------------------------------
Funcion que escribe en un archivo la imagen de salida.
Entrada: Nombre archivo imagen salida, número de ángulos, número de distancias y una matriz.
Salida: Nada.
*/
void writeImage( char *image_hough_str, int angles, int distances, int **matrix);

/*---------------------------------------------------------------------------------------------------
Funcion que detecta las rectas de una imagen almacenada en una matriz. Se ocupa la transformada de Hough.
Entrada: Número de filas y columnas, una matriz con la imagen, número de ángulos y distancias, matriz donde se guardará el resultado y delta theta.
Salida: Matriz con resultado de la transformada.
*/
int** lineDetection(int rows, int columns, int **image_matrix, int angles, int distances, int **H_matrix_normal, double delta_theta);

/*---------------------------------------------------------------------------------------------------
Funcion de umbralización, permite mostrar las ractas más relevantes de una imagen.
Entrada: Número de ángulos y distancias, matriz con imagen ya transformada, núemro del umbral.
Salida: Nada.
*/
void tresholding(int angles, int distances, int **H_matrix_normal, int threshold);

/*---------------------------------------------------------------------------------------------------
Funcion que detecta las rectas de una imagen almacenada una matriz, esto haciendo uso de registros (SIMD). Se ocupa la transformada de Hough.
Entrada: Número de filas y columnas, una matriz con la imagen, número de ángulos y distancias, matriz donde se guardará el resultado y delta theta.
Salida: Matriz con resultado de la transformada.
*/
int** lineDetectionSIMD(int rows, int columns, int **image_matrix, int angles, int distances, int **H_matrix_simd, double delta_theta);

/*---------------------------------------------------------------------------------------------------
Funcion que imprime por pantalla el ángulo y la distancia de las rectas encontradas.
Entrada: Número de ángulos y distancias, matriz con imagen transformada, delta theta y delta r.
Salida: Nada.
*/
void writeLines(int angles, int distances, int **H_matrix, int threshold, double delta_theta, double delta_r);