/********************************************************************************/
/*					Departamento de Ingenieria Informatica 						*/
/*						Universidad de Santiago de Chile						*/
/*						    High Performance Computing		     			    */
/*						 	     Laboratorio 1									*/
/********************************************************************************/
/*		  Autores: Gustavo Hurtado Arévalo y Patricia Melo Fuenzalida.			*/
/********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <getopt.h>
//#include <pthread.h>
#include <fcntl.h>
#include <unistd.h>
#include <xmmintrin.h> /* SSE __m128 float */

#include "cabeceras.h"

int validateEntry(char *image_raw_str, char *image_hough_str, int rows, int columns, int angles, int distances, int threshold)
{
    //Validaciones de entradas
    if (open(image_raw_str, O_RDONLY) == -1)
    {
        printf("ERROR. Archivo de entrada no existe.\n");
        return -1;
    }
    else if (open(image_hough_str, O_CREAT, 0777) == -1)
    {
        printf("ERROR. No se pudo crear archivo de salida.\n");
        return -1;
    }
    else if (rows <= 0)
    {
        printf("ERROR. Número de filas de imagen inválido.\n");
        return -1;
    }
    else if (columns <= 0)
    {
        printf("ERROR. Número de columnas de imagen inválido.\n");
        return -1;
    }
    else if (angles <= 0)
    {
        printf("ERROR. Número de ángulos inválido.\n");
        return -1;
    }
    else if (distances <= 0)
    {
        printf("ERROR. Número de distancias inválido.\n");
        return -1;
    }
    else if (threshold <= 0)
    {
        printf("ERROR. Umbral para detección de líneas inválido.\n");
        return -1;
    }
    else
    {
        return 0;
    }
}

int **giveMemorieMatrix(int rows, int columns)
{
    // Memoria para image_matrix
    int **matrix = (int **)malloc(rows * sizeof(int *));
    for (size_t i = 0; i < rows; i++)
    {
        matrix[i] = (int *)malloc(columns * sizeof(int));
        for (size_t j = 0; j < columns; j++)
        {
            matrix[i][j] = 0;
        }
    }
    return matrix;
}

int **readImage(int **image_matrix, char *image_raw_str, int rows, int columns)
{
    int fd_image_raw;

    fd_image_raw = open(image_raw_str, O_RDONLY);

    // Lectura de la imagen
    for (int i = 0; i < rows; ++i)
    {
        read(fd_image_raw, image_matrix[i], columns * sizeof(int));
    }
    close(fd_image_raw);
    return image_matrix;
}

void writeImage(char *image_hough_str, int angles, int distances, int **matrix)
{
    int fd_image_hough;

    fd_image_hough = open(image_hough_str, O_WRONLY, 0777);

    for (size_t i = 0; i < angles; i++)
    {
        write(fd_image_hough, matrix[i], distances * sizeof(int));
    }
    close(fd_image_hough);
}

int **lineDetection(int rows, int columns, int **image_matrix, int angles, int distances, int **H_matrix_normal, double delta_theta)
{
    double theta;
    int r_j;
    for (size_t x = 0; x < rows; x++)
    {
        for (size_t y = 0; y < columns; y++)
        {
            if (image_matrix[x][y] > 0)
            {
                for (size_t i = 0; i < angles; i++)
                {
                    theta = delta_theta * i;
                    r_j = (int)((x * cos(theta)) + (y * sin(theta)));
                    H_matrix_normal[i][distances / 2 - r_j] = H_matrix_normal[i][distances / 2 - r_j] + 1;
                }
            }
        }
    }
    return H_matrix_normal;
}

int ** thresholding(int angles, int distances, int **H_matrix_normal, int threshold)
{
    for (size_t i = 0; i < angles; i++)
    {
        for (size_t j = 0; j < distances; j++)
        {
            if (H_matrix_normal[i][j] < threshold)
            {
                H_matrix_normal[i][j] = 0;
            }
        }
    }

    return H_matrix_normal;
}

int **lineDetectionSIMD(int rows, int columns, int **image_matrix, int angles, int distances, int **H_matrix_simd, double delta_theta)
{
    float buffer[4] __attribute__((aligned(16))) = {0.0, 0.0, 0.0, 0.0};
    float buffer2[4] __attribute__((aligned(16))) = {0.0, 0.0, 0.0, 0.0};

    __m128 r_sin, r_cos, r_x, r_y, r_x_mul, r_y_mul, r_sum, r_i, r_delta_theta, r_i_mul;

    if (angles % 4 == 0)
    {
        for (size_t x = 0; x < rows; x++)
        {
            for (size_t y = 0; y < columns; y++)
            {
                if (image_matrix[x][y] > 0)
                {
                    for (size_t i = 0; i < angles; i += 4)
                    {
                        r_delta_theta = _mm_set1_ps(delta_theta);
                        r_i = _mm_set_ps(i, i + 1, i + 2, i + 3);
                        r_i_mul = _mm_mul_ps(r_delta_theta, r_i);
                        _mm_store_ps(buffer2, r_i_mul);

                        r_cos = _mm_set_ps(cos(buffer2[0]), cos(buffer2[1]), cos(buffer2[2]), cos(buffer2[3]));
                        r_sin = _mm_set_ps(sin(buffer2[0]), sin(buffer2[1]), sin(buffer2[2]), sin(buffer2[3]));

                        r_x = _mm_set1_ps(x);
                        r_y = _mm_set1_ps(y);

                        r_x_mul = _mm_mul_ps(r_cos, r_x);
                        r_y_mul = _mm_mul_ps(r_sin, r_y);

                        r_sum = _mm_add_ps(r_x_mul, r_y_mul);
                        _mm_store_ps(buffer, r_sum);

                        H_matrix_simd[i][distances / 2 - (int)buffer[0]] = H_matrix_simd[i][distances / 2 - (int)buffer[0]] + 1;
                        H_matrix_simd[i + 1][distances / 2 - (int)buffer[1]] = H_matrix_simd[i + 1][distances / 2 - (int)buffer[1]] + 1;
                        H_matrix_simd[i + 2][distances / 2 - (int)buffer[2]] = H_matrix_simd[i + 2][distances / 2 - (int)buffer[2]] + 1;
                        H_matrix_simd[i + 3][distances / 2 - (int)buffer[3]] = H_matrix_simd[i + 3][distances / 2 - (int)buffer[3]] + 1;
                    }
                }
            }
        }
    }

    else
    {
        for (size_t x = 0; x < rows; x++)
        {
            for (size_t y = 0; y < columns; y++)
            {
                if (image_matrix[x][y] > 0)
                {
                    for (size_t i = 0; i < (((angles) / 4) * 4); i += 4)
                    {
                        r_delta_theta = _mm_set1_ps(delta_theta);
                        r_i = _mm_set_ps(i, i + 1, i + 2, i + 3);
                        r_i_mul = _mm_mul_ps(r_delta_theta, r_i);
                        _mm_store_ps(buffer2, r_i_mul);

                        r_cos = _mm_set_ps(cos(buffer2[0]), cos(buffer2[1]), cos(buffer2[2]), cos(buffer2[3]));
                        r_sin = _mm_set_ps(sin(buffer2[0]), sin(buffer2[1]), sin(buffer2[2]), sin(buffer2[3]));

                        r_x = _mm_set1_ps(x);
                        r_y = _mm_set1_ps(y);

                        r_x_mul = _mm_mul_ps(r_cos, r_x);
                        r_y_mul = _mm_mul_ps(r_sin, r_y);

                        r_sum = _mm_add_ps(r_x_mul, r_y_mul);
                        _mm_store_ps(buffer, r_sum);

                        H_matrix_simd[i][distances / 2 - (int)buffer[0]] = H_matrix_simd[i][distances / 2 - (int)buffer[0]] + 1;
                        H_matrix_simd[i + 1][distances / 2 - (int)buffer[1]] = H_matrix_simd[i + 1][distances / 2 - (int)buffer[1]] + 1;
                        H_matrix_simd[i + 2][distances / 2 - (int)buffer[2]] = H_matrix_simd[i + 2][distances / 2 - (int)buffer[2]] + 1;
                        H_matrix_simd[i + 3][distances / 2 - (int)buffer[3]] = H_matrix_simd[i + 3][distances / 2 - (int)buffer[3]] + 1;
                    }

                    for (size_t i = (((angles) / 4) * 4); i < angles; i++)
                    {
                        r_delta_theta = _mm_set_ss(delta_theta);
                        r_i = _mm_set_ss(i);
                        r_i_mul = _mm_mul_ps(r_delta_theta, r_i);
                        _mm_store_ps(buffer2, r_i_mul);

                        r_cos = _mm_set_ss(cos(buffer2[0]));
                        r_sin = _mm_set_ss(sin(buffer2[0]));

                        r_x = _mm_set_ss(x);
                        r_y = _mm_set_ss(y);

                        r_x_mul = _mm_mul_ss(r_cos, r_x);
                        r_y_mul = _mm_mul_ss(r_sin, r_y);

                        r_sum = _mm_add_ss(r_x_mul, r_y_mul);
                        _mm_store_ss(buffer, r_sum);

                        H_matrix_simd[i][distances / 2 - (int)buffer[0]] = H_matrix_simd[i][distances / 2 - (int)buffer[0]] + 1;
                    }
                }
            }
        }
    }

    return H_matrix_simd;
}

void writeLines(int angles, int distances, int **H_matrix, int threshold, double delta_theta, double delta_r)
{

    for (size_t i = 0; i < angles; i++)
    {
        for (size_t j = 0; j < distances; j++)
        {
            if (H_matrix[i][j] > threshold)
            {
                printf("Theta: %i, Distancia: %f \n", (int)((i * delta_theta * 180) / M_PI), j * delta_r);
            }
        }
    }
}

void writeTime(clock_t initial ,clock_t end){
    double total = (double)(end - initial) / CLOCKS_PER_SEC;
    printf("Tiempo[s]: %f \n", total);
}

int main(int argc, char *argv[])
{
    if (argc >= 15)
    {
        int **image_matrix, **H_matrix_normal, **H_matrix_simd;
        clock_t secuential_start_t, secuential_end_t, simd_start_t, simd_end_t;

        int value = 0;
        double delta_theta, delta_r;

        char *image_raw_str = NULL;   //imagen entrada.
        char *image_hough_str = NULL; //imagen salida.
        int rows = 0;                 //nro filas imagen
        int columns = 0;              //nro columnas imagen.
        int angles = 0;               //nro de ángulos.
        int distances = 0;            //nro de distancias.
        int threshold = 0;            //umbral para detección de líneas.
        int c;                        //retorno getopt

        //Getopt
        while ((c = getopt(argc, argv, "i:o:M:N:T:R:U:")) != -1)
        {
            switch (c)
            {
            case 'i':
                image_raw_str = optarg; //nombre imagen entrada.
                break;
            case 'o':
                image_hough_str = optarg; //nombre imagen salida.
                break;
            case 'M':
                rows = atoi(optarg); //nro filas imagen.
                break;
            case 'N':
                columns = atoi(optarg); //nro columnas imagen.
                break;
            case 'T':
                angles = atoi(optarg); //nro de ángulos.
                break;
            case 'R':
                distances = atoi(optarg); //nro de distancias.
                break;
            case 'U':
                threshold = atoi(optarg); //umbral para detección de líneas.
                break;
            default:
                abort();
            }
        }

        //Validaciones de entradas
        value = validateEntry(image_raw_str, image_hough_str, rows, columns, angles, distances, threshold);
        if (value != 0)
            return -1;

        delta_r = (sqrt(pow(rows, 2) + pow(columns, 2))) / (2 * (double)distances);
        delta_theta = M_PI / (double)angles;

        // Memoria para image_matrix
        image_matrix = giveMemorieMatrix(rows, columns);

        // Lectura imagen
        image_matrix = readImage(image_matrix, image_raw_str, rows, columns);

        // =========== Sin simd ===========

        // Memoria para matriz de hough sin simd
        H_matrix_normal = giveMemorieMatrix(angles, distances);

        secuential_start_t = clock();
        // Detección de líneas
        H_matrix_normal = lineDetection(rows, columns, image_matrix, angles, distances, H_matrix_normal, delta_theta);
        secuential_end_t = clock();

        // =========== Con simd ===========


        // Memoria para matriz de hough con simd
        H_matrix_simd = giveMemorieMatrix(angles, distances);

        simd_start_t = clock();
        // Detección de líneas con SIMD
        H_matrix_simd = lineDetectionSIMD(rows, columns, image_matrix, angles, distances, H_matrix_simd, delta_theta);
        simd_end_t = clock();

        // Umbralización
        H_matrix_simd = thresholding(angles, distances, H_matrix_normal, threshold);

        // Escribir imagen salida
        writeImage(image_hough_str, angles, distances, H_matrix_simd);

        // Salida de texto para SIMD
        printf("\n\n=============== Salidas del programa sin SIMD ===============\n");
        writeTime(secuential_start_t, secuential_end_t);
        writeLines(angles, distances, H_matrix_normal, threshold, delta_theta, delta_r);
        printf("=============== Fin salidas del programa sin SIMD ===========\n\n");


        printf("===============  Salidas del programa con SIMD ==============\n");
        // Salida de texto para SIMD
        writeTime(simd_start_t, simd_end_t);
        writeLines(angles, distances, H_matrix_simd, threshold, delta_theta, delta_r);
        printf("===============  Fin salidas del programa con SIMD ==========\n\n");

    }
    else
    {
        printf("ERROR. Faltan parámetros.\n");
        printf("Uso: ./hough -i imagen.raw -o hough.raw -M tamañoX -N tamañoY -T numero_angulos -R numero_distancias -U umbral\n");
    }

    return (0);
}