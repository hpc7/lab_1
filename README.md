/********************************************************************************/
/*					Departamento de Ingenieria Informatica 						*/
/*						Universidad de Santiago de Chile						*/
/*						    High Performance Computing		     			    */
/*						 	     Laboratorio 1									*/
/********************************************************************************/
/*		  Autores: Gustavo Hurtado Arévalo y Patricia Melo Fuenzalida.			*/
/********************************************************************************/

## Compilación del programa

Para la compilar es necesario hacer uso del comando make en el mismo directorio.

## Ejecución del programa

Ejemplo de ejecución

./hough -i circuit1-280x272.raw -o image_hough.raw -M 280 -N 272 -T 512 -R 1000 -U 100

## Consideraciones

Es necesario tener en cuenta que dependiendo del tamaño que tengan las imágenes de entrada
existe una distancia mínima a la que el programa funciona sin problemas. Existen casos como
en el de simplehough1-256x256 las distancias mínimas son de 500, por lo que cualquier número
sobre ese tendrá los resultados adecuados. En caso de que se ingrese una distancia menor
el programa arrojará un segmention fault.

Buscamos con valgrind dónde encontraba el error e indica que es al acceder a la matriz H,
sin embargo, revisamos la asignación de memoria y se encuentra correcta.
